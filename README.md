# fw_template

FW base structure

# Folder structure

* IDE:      All IDE related files.
* library   All libreries used in project (vendor and user).
* CMSIS     CMSIS device files (.s, .icf, system_.xxxc).
* app       Application source files.
